package script

import (
	"encoding/csv"
	"fmt"
	"full-text-search/logs"
	meili "full-text-search/meli"
	"full-text-search/models"
	"io"
	"os"
	"time"
)

func InsertMockToMeliSearch() {
	indexMeli := meili.Client().Index("onechat")
	for k := 1; k <= 30; k++ {
		for i := 1; i <= 10; i++ {
			f, err := os.Open(fmt.Sprintf("mock_data/message_temp_%v/messages_%v.csv", k, i))
			if err != nil {
				logs.Error(err)
				return
			}
			defer f.Close()

			// Create a CSV reader.
			reader := csv.NewReader(f)

			// Read the first row of the file.
			header, err := reader.Read()
			_ = header
			if err != nil {
				logs.Error(err)
				return
			}

			// Iterate over the rows of the file.
			var messages []models.MessageInMeli
			index := 1
			for {
				// Read a row.
				record, err := reader.Read()
				if err == io.EOF {
					logs.Error(err)
					break
				}

				// Print the values of the columns.
				timeObj, err := time.Parse("2006-01-02 15:04:05", record[2])
				if err != nil {
					logs.Error(err)
					return
				}

				messageData := models.MessageInMeli{
					MessageId: record[0],
					SenderId:  "1",
					Message:   record[1],
					RoomId:    record[3],
					TimeStamp: timeObj.UnixNano(),
				}

				messages = append(messages, messageData)
				if len(messages) == 10000 {
					task, err := indexMeli.AddDocuments(messages)
					_ = task
					if err != nil {
						logs.Error(err)
						return
					}
					messages = []models.MessageInMeli{}
					fmt.Println("len(messages): ", len(messages), task.TaskUID, index, k, i)
				}
				index = index + 1
			}

			// Insert any remaining messages.
			if len(messages) > 0 {
				task, err := indexMeli.AddDocuments(messages)
				if err != nil {
					logs.Error(err)
					return
				}
				fmt.Println(task.TaskUID)
			}

			fmt.Println("< --- done --- >")
		}
	}
}

import csv
import random
from faker import Faker

fake_th = Faker("th_TH")
fake_en = Faker("en_US")

def generate_message():
    language = random.choice(["th", "en"])
    fake = fake_th if language == "th" else fake_en

    return {
        "message_id": fake.uuid4(),
        "content": fake.text(),
        "timestamp": fake.date_time_this_year(),
        "room": random.choice(["r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10"]),
    }

records_per_file = 100000
num_files = 10

for file_idx in range(num_files):
    filename = f"messages_{file_idx + 1}.csv"

    with open(filename, "w", newline="", encoding="utf-8") as csvfile:
        fieldnames = ["message_id", "content", "timestamp", "room"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for _ in range(records_per_file):
            writer.writerow(generate_message())

    print(f"Generated {filename}")
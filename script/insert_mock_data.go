package script

import (
	"encoding/csv"
	"fmt"
	"full-text-search/database"
	"full-text-search/logs"
	"full-text-search/models"
	uuid "github.com/satori/go.uuid"
	"io"
	"math/rand"
	"os"
	"time"
)

func InsertMockData() {
	for k := 23; k <= 30; k++ {
		for i := 1; i <= 10; i++ {
			f, err := os.Open(fmt.Sprintf("mock_data/message_temp_%v/messages_%v.csv", k, i))
			if err != nil {
				fmt.Println(err)
				return
			}
			defer f.Close()

			// Create a CSV reader.
			reader := csv.NewReader(f)

			// Read the first row of the file.
			header, err := reader.Read()
			_ = header
			if err != nil {
				logs.Error(err)
				return
			}

			// Iterate over the rows of the file.
			var messages []models.Message
			db := database.DBConn
			index := 1
			for {
				// Read a row.
				record, err := reader.Read()
				if err == io.EOF {
					logs.Error(err)
					break
				}

				// Print the values of the columns.
				timeObj, err := time.Parse("2006-01-02 15:04:05", record[2])
				if err != nil {
					logs.Error(err)
					return
				}
				myUuid, err := uuid.FromString(record[0])
				if err != nil {
					logs.Error(err)
					return
				}

				messageData := models.Message{
					Model: models.Model{
						Uid:       myUuid,
						Seq:       timeObj.UnixNano(),
						CreatedAt: timeObj,
						UpdatedAt: timeObj,
					},
					SenderId: "1",
					RoomId:   record[3],
					Message:  record[1],
				}

				messages = append(messages, messageData)
				index = index + 1
				if len(messages) == 1000 {
					err := db.Create(&messages).Error
					if err != nil {
						logs.Error(err)
						return
					}
					fmt.Println(index, record[0])
					messages = []models.Message{}
				}
			}

			// Insert any remaining messages.
			if len(messages) > 0 {
				err := db.Create(&messages).Error
				if err != nil {
					logs.Error(err)
					return
				}
			}

			fmt.Println("< --- done --- >")
		}
	}
}

func getRandomNumber() int {
	// Generate a random number between 1 and 10.
	randomNumber := rand.Intn(10) + 1

	// Return the random number.
	return randomNumber
}

package database

import (
	"context"
	"fmt"
	"full-text-search/logs"
	"full-text-search/models"
	"time"

	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	DBConn *gorm.DB
)

type SqlLogger struct {
	logger.Interface
}

func (l SqlLogger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	sql, _ := fc() // ตัว fc จะทำการแสดงผลของตัวคำสั่งใน GORM ออกมาเป็น Query
	fmt.Printf("%v\n--------------------------------------\n", sql)
}

func InitDatabase() {
	fmt.Println("host", viper.GetString("pg.host"))
	var err error
	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=disable TimeZone=Asia/Bangkok", viper.GetString("pg.host"), viper.GetString("pg.username"), viper.GetString("pg.password"), viper.GetString("pg.name"), viper.GetString("pg.port"))
	DBConn, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
		// Logger: &SqlLogger{},
	})
	if err != nil {
		fmt.Println(err)
		panic("Failed to connect to database")
	}
	fmt.Println("Database connection successfully")

	if err := DBConn.AutoMigrate(
		&models.Message{},
	); err != nil {
		logs.Error(err)
	}
}

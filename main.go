package main

import (
	"fmt"
	"full-text-search/api"
	"full-text-search/database"
	"full-text-search/logs"
	"full-text-search/meli"
	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
	"os"
	"strings"
	"time"

	_ "full-text-search/docs"
	_ "time/tzdata"

	swagger "github.com/arsmn/fiber-swagger/v2"
)

// @title Full Text Search Example
// @version 1.0
// @description Full Text Search Example
// @contact.name OneCentric
// @contact.email onecentric@inet.co.th
// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	initConfig()
	initTimeZone()
	meili.InitMeiliSearch()
	database.InitDatabase()

	//* insert mock data to pg
	//script.InsertMockData()
	//return

	//* insert mock data to melisearch
	//script.InsertMockToMeliSearch()
	//return

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	app.Post("/send-message", api.SendMessage)
	app.Post("/search", api.Search)
	app.Put("/update", api.Update)
	app.Delete("/delete", api.Delete)

	app.Get("/docs/*", swagger.New(swagger.Config{
		DeepLinking: true,
		URL:         "doc.json",
	}))

	if err := app.Listen(fmt.Sprintf(":%v", viper.GetInt("app.port"))); err != nil {
		logs.Error(err)
	}

}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}

	time.Local = ict
}

func initConfig() {
	switch os.Getenv("ENV") {
	case "":
		os.Setenv("ENV", "dev")
		viper.SetConfigName("config_dev")
	case "uat-local":
		os.Setenv("ENV", "uat")
		viper.SetConfigName("config_dev_uat")
	case "prd-local":
		os.Setenv("ENV", "prd")
		viper.SetConfigName("config_dev_prd")
	default:
		viper.SetConfigName("config")
	}

	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

// Package docs GENERATED BY SWAG; DO NOT EDIT
// This file was generated by swaggo/swag
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {
            "name": "OneCentric",
            "email": "onecentric@inet.co.th"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/delete": {
            "delete": {
                "description": "API delete",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FullTextSearch"
                ],
                "summary": "API delete",
                "parameters": [
                    {
                        "description": "ใส่ message id ที่ต้องการ delete",
                        "name": "JSON",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/api.reqDelete"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseSuccessNoData"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError400"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError401"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError"
                        }
                    }
                }
            }
        },
        "/search": {
            "post": {
                "description": "API ค้นหา",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FullTextSearch"
                ],
                "summary": "API ค้นหา",
                "parameters": [
                    {
                        "description": "ใส่คำค้นหา",
                        "name": "JSON",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/api.reqSearch"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseSuccessNoData"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError400"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError401"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError"
                        }
                    }
                }
            }
        },
        "/send-message": {
            "post": {
                "description": "API ทดสอบเพิ่ม message",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FullTextSearch"
                ],
                "summary": "API ทดสอบเพิ่ม message",
                "parameters": [
                    {
                        "description": "ทดสอบเพิ่ม message",
                        "name": "JSON",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/api.requestSendMessage"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseSuccessNoData"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError400"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError401"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError"
                        }
                    }
                }
            }
        },
        "/update": {
            "put": {
                "description": "API update",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FullTextSearch"
                ],
                "summary": "API update",
                "parameters": [
                    {
                        "description": "ใส่ข้อมูลที่ต้องการ update",
                        "name": "JSON",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.MessageInMeli"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseSuccessNoData"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError400"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError401"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/models.ResponseError"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "api.reqDelete": {
            "type": "object",
            "properties": {
                "message_id": {
                    "type": "string"
                }
            }
        },
        "api.reqSearch": {
            "type": "object",
            "properties": {
                "search_text": {
                    "type": "string",
                    "example": "hello"
                }
            }
        },
        "api.requestSendMessage": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string",
                    "example": "Hello Golang"
                },
                "room_id": {
                    "type": "string",
                    "example": "r1"
                },
                "user_id": {
                    "type": "string",
                    "example": "1"
                }
            }
        },
        "models.MessageInMeli": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string",
                    "example": "hello world"
                },
                "message_id": {
                    "type": "string",
                    "example": "fba41712-82af-404b-9e8e-7f5c17910458"
                },
                "room_id": {
                    "type": "string",
                    "example": "r1"
                },
                "sender_id": {
                    "type": "string",
                    "example": "1"
                },
                "time_stamp": {
                    "type": "integer",
                    "example": 1682677607431370000
                }
            }
        },
        "models.ResponseError": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string",
                    "example": "internal_server_error"
                },
                "status": {
                    "type": "integer",
                    "example": 500
                }
            }
        },
        "models.ResponseError400": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string",
                    "example": "missing_required_parameter"
                },
                "status": {
                    "type": "integer",
                    "example": 400
                }
            }
        },
        "models.ResponseError401": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string",
                    "example": "unauthorized"
                },
                "status": {
                    "type": "integer",
                    "example": 401
                }
            }
        },
        "models.ResponseSuccessNoData": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string",
                    "example": "success"
                },
                "status": {
                    "type": "integer",
                    "example": 200
                }
            }
        }
    },
    "securityDefinitions": {
        "ApiKeyAuth": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "1.0",
	Host:             "",
	BasePath:         "/",
	Schemes:          []string{},
	Title:            "Full Text Search Example",
	Description:      "Full Text Search Example",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}

package meili

import (
	"github.com/meilisearch/meilisearch-go"
	"github.com/spf13/viper"
)

var client *meilisearch.Client

func InitMeiliSearch() {
	client = meilisearch.NewClient(meilisearch.ClientConfig{
		Host:   viper.GetString("meli.host"),
		APIKey: viper.GetString("meli.master_key"),
	})

	client.CreateIndex(&meilisearch.IndexConfig{
		Uid:        "onechat",
		PrimaryKey: "message_id",
	})

	//* ถ้าจะแก้ config ต้อง reset ก่อนหนึ่งรอบ และ update setting ใหม่
	// client.Index("onechat").ResetSettings()

	settings := meilisearch.Settings{
		SearchableAttributes: []string{
			"message",
		},
		SortableAttributes: []string{
			"time_stamp",
		},
		FilterableAttributes: []string{
			"room_id",
			"sender_id",
		},
	}

	client.Index("onechat").UpdateSettings(&settings)
}

func Client() *meilisearch.Client {
	return client
}

package models

type Message struct {
	Model
	SenderId string `json:"sender_id"`
	RoomId   string `json:"room_id"`
	Message  string `json:"message"`
}

package models

type ResponseSuccess struct {
	Status  int         `json:"status" example:"200"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"  example:"success"`
}

type ResponseSuccessNoData struct {
	Status  int    `json:"status" example:"200"`
	Message string `json:"message"  example:"success"`
}

type ResponseError struct {
	Status  int    `json:"status" example:"500"`
	Message string `json:"message" example:"internal_server_error"`
}

type ResponseError400 struct {
	Status  int    `json:"status" example:"400"`
	Message string `json:"message" example:"missing_required_parameter"`
}

type ResponseError404 struct {
	Status  int    `json:"status" example:"404"`
	Message string `json:"message" example:"one_id_not_found"`
}

type ResponseError401 struct {
	Status  int    `json:"status" example:"401"`
	Message string `json:"message" example:"unauthorized"`
}

type ResponseError401Miss struct {
	Status  int    `json:"status" example:"401"`
	Message string `json:"message" example:"missing_or_malformed_jwt"`
}

type ResponseError401Expire struct {
	Status  int    `json:"status" example:"401"`
	Message string `json:"message" example:"invalid_or_expired_jwt"`
}

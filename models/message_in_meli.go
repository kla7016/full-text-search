package models

type MessageInMeli struct {
	MessageId string `json:"message_id" example:"fba41712-82af-404b-9e8e-7f5c17910458"`
	SenderId  string `json:"sender_id" example:"1"`
	Message   string `json:"message" example:"hello world"`
	RoomId    string `json:"room_id" example:"r1"`
	TimeStamp int64  `json:"time_stamp" example:"1682677607431370000"`
}

FROM golang:1.19 AS build
COPY ./ /app/
WORKDIR /app/
RUN CGO_ENABLED=0 GOOS=linux go build -o /bin/full-text-search

FROM alpine:3.12
ENV TZ=Asia/Bangkok
COPY --from=build /bin/full-text-search /
COPY ./config.yaml /config.yaml
COPY ./Makefile /Makefile
COPY ./docs /docs
EXPOSE 3000
CMD ["/full-text-search"]

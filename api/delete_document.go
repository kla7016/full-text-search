package api

import (
	meili "full-text-search/meli"
	"full-text-search/utility"
	"github.com/gofiber/fiber/v2"
)

type reqDelete struct {
	MessageId string `json:"message_id"`
}

// Delete
// @Summary API delete
// @Description  API delete
// @Tags FullTextSearch
// @Accept json
// @Produce json
// @Param JSON body reqDelete true "ใส่ message id ที่ต้องการ delete"
// @Success 200 {object} models.ResponseSuccessNoData
// @Failure 400 {object} models.ResponseError400
// @Failure 401 {object} models.ResponseError401
// @Failure 500 {object} models.ResponseError
// @Router /delete [delete]
func Delete(c *fiber.Ctx) error {
	reqBody := new(reqDelete)
	if err := c.BodyParser(reqBody); err != nil {
		return utility.ResponseError(c, fiber.StatusBadRequest, err.Error())
	}
	meili.Client().Index("onechat").DeleteDocument(reqBody.MessageId)

	return utility.ResponseSuccess(c, nil)
}

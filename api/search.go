package api

import (
	"fmt"
	meili "full-text-search/meli"
	"full-text-search/utility"
	"github.com/gofiber/fiber/v2"
	"github.com/meilisearch/meilisearch-go"
)

type reqSearch struct {
	SearchText string `json:"search_text" example:"hello"`
}

// Search
// @Summary API ค้นหา
// @Description  API ค้นหา
// @Tags FullTextSearch
// @Accept json
// @Produce json
// @Param JSON body reqSearch true "ใส่คำค้นหา"
// @Success 200 {object} models.ResponseSuccessNoData
// @Failure 400 {object} models.ResponseError400
// @Failure 401 {object} models.ResponseError401
// @Failure 500 {object} models.ResponseError
// @Router /search [post]
func Search(c *fiber.Ctx) error {
	reqBody := new(reqSearch)
	if err := c.BodyParser(reqBody); err != nil {
		return utility.ResponseError(c, fiber.StatusBadRequest, err.Error())
	}

	fmt.Println(reqBody.SearchText)

	//fmt.Println(meili.Client().Index("onechat").GetFilterableAttributes()) //* GetFilterAble
	//fmt.Println(meili.Client().Index("onechat").GetStats()) //* GetTotalsDocument

	searchRes, err := meili.Client().Index("onechat").Search(reqBody.SearchText,
		&meilisearch.SearchRequest{
			Limit: 10,
			//Filter: "",
			Offset: 0,
			Filter: "room_id IN [r2, r1]",
			Sort:   []string{"time_stamp:desc"},
		})

	if err != nil {
		return utility.ResponseError(c, fiber.StatusInternalServerError, err.Error())
	}

	return c.JSON(searchRes)
}

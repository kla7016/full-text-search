package api

import (
	"fmt"
	meili "full-text-search/meli"
	"full-text-search/models"
	"full-text-search/utility"
	"github.com/gofiber/fiber/v2"
	uuid "github.com/satori/go.uuid"
	"time"
)

type requestSendMessage struct {
	UserId  string `json:"user_id" example:"1"`
	Message string `json:"message" example:"Hello Golang"`
	RoomId  string `json:"room_id" example:"r1"`
}

// SendMessage
// @Summary API ทดสอบเพิ่ม message
// @Description  API ทดสอบเพิ่ม message
// @Tags FullTextSearch
// @Accept json
// @Produce json
// @Param JSON body requestSendMessage true "ทดสอบเพิ่ม message"
// @Success 200 {object} models.ResponseSuccessNoData
// @Failure 400 {object} models.ResponseError400
// @Failure 401 {object} models.ResponseError401
// @Failure 500 {object} models.ResponseError
// @Router /send-message [post]
func SendMessage(c *fiber.Ctx) error {
	reqBody := new(requestSendMessage)
	if err := c.BodyParser(reqBody); err != nil {
		return utility.ResponseError(c, fiber.StatusBadRequest, err.Error())
	}

	messageId := uuid.NewV4()
	_ = messageId

	index := meili.Client().Index("onechat")

	documents := models.MessageInMeli{
		MessageId: messageId.String(),
		SenderId:  reqBody.UserId,
		Message:   reqBody.Message,
		RoomId:    reqBody.RoomId,
		TimeStamp: time.Now().UnixNano(),
	}
	//documents := []map[string]interface{}{
	//	{"id": 1, "message": reqBody.Message, "user_id": reqBody.UserId, "room_id": reqBody.RoomId},
	//}

	//documents := []map[string]interface{}{
	//	// {"message_id": messageId.String(), "user_id": reqBody.UserId, "message": reqBody.Message, "room_id": reqBody.RoomId},
	//	{"id": 0, "message": reqBody.Message, "room_id": reqBody.RoomId, "user_id": reqBody.UserId},
	//}

	fmt.Println(documents)
	task, err := index.AddDocuments(documents)
	if err != nil {
		fmt.Println(err)
		return utility.ResponseError(c, fiber.StatusInternalServerError, err.Error())
	}

	fmt.Println(task.TaskUID)

	return utility.ResponseSuccess(c, fiber.Map{
		"task_uid":   task.TaskUID,
		"message_id": messageId,
	})
}

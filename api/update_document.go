package api

import (
	meili "full-text-search/meli"
	"full-text-search/models"
	"full-text-search/utility"
	"github.com/gofiber/fiber/v2"
)

// Update
// @Summary API update
// @Description  API update
// @Tags FullTextSearch
// @Accept json
// @Produce json
// @Param JSON body models.MessageInMeli true "ใส่ข้อมูลที่ต้องการ update"
// @Success 200 {object} models.ResponseSuccessNoData
// @Failure 400 {object} models.ResponseError400
// @Failure 401 {object} models.ResponseError401
// @Failure 500 {object} models.ResponseError
// @Router /update [put]
func Update(c *fiber.Ctx) error {
	reqBody := new(models.MessageInMeli)
	if err := c.BodyParser(reqBody); err != nil {
		return utility.ResponseError(c, fiber.StatusBadRequest, err.Error())
	}

	documents := models.MessageInMeli{
		MessageId: reqBody.MessageId,
		SenderId:  reqBody.SenderId,
		Message:   reqBody.Message,
		RoomId:    reqBody.RoomId,
		TimeStamp: reqBody.TimeStamp,
	}
	meili.Client().Index("onechat").UpdateDocuments(documents)

	return utility.ResponseSuccess(c, nil)
}
